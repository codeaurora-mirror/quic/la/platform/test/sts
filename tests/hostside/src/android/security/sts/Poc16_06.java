/**
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package android.security.sts;

import android.platform.test.annotations.RootPermissionTest;
import com.android.tradefed.testtype.DeviceJUnit4ClassRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(DeviceJUnit4ClassRunner.class)
public class Poc16_06 extends SecurityTestCase {

    /**
     *  b/27407865
     */
    @RootPermissionTest(minPatchLevel = "2016-06")
    @Test
    public void testPocCVE_2016_2465() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/sys/kernel/debug/mdp/perf/perf_mode")) {
            AdbUtils.runPoc("CVE-2016-2465", getDevice(), 60);
        }
    }

    /**
     *  b/27407629
     */
    @RootPermissionTest(minPatchLevel = "2016-06")
    @Test
    public void testPocCVE_2016_2489() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/sys/kernel/debug/mdp/perf/disable_panic")) {
            AdbUtils.runPoc("CVE-2016-2489", getDevice(), 60);
        }
    }
}
