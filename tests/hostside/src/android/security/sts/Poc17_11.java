/**
 * Copyright (C) 2017 The Android Open Source Project
 *
 * <p>Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 * <p>http://www.apache.org/licenses/LICENSE-2.0
 *
 * <p>Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */
package android.security.sts;

import android.platform.test.annotations.RootPermissionTest;
import com.android.tradefed.testtype.DeviceJUnit4ClassRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(DeviceJUnit4ClassRunner.class)
public class Poc17_11 extends SecurityTestCase {

  /** b/34705430 */
  @RootPermissionTest
  @Test
  public void testPocCVE_2017_6264() throws Exception {
    enableAdbRoot(getDevice());
    if (containsDriver(getDevice(), "/sys/devices/virtual/thermal/cooling_device2/cur_state")) {
      AdbUtils.runPocNoOutput("CVE-2017-6264", getDevice(), 60);
    }
  }

  /** b/36575870 */
  @RootPermissionTest
  @Test
  public void testPocCVE_2017_9690() throws Exception {
    enableAdbRoot(getDevice());
    if (containsDriver(getDevice(), "/dev/qbt1000")) {
      AdbUtils.runPoc("CVE-2017-9690", getDevice(), 60);
    }
  }
}
