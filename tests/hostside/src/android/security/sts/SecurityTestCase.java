/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.security.sts;

import static org.junit.Assert.*;

import com.android.ddmlib.Log.LogLevel;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.device.NativeDevice;
import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.testtype.IDeviceTest;
import java.util.regex.Pattern;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;

public class SecurityTestCase implements IDeviceTest {

  private long kernelStartTime;
  // JUnit requires @AfterClass methods to be static, so this can't be an instance variable.
  // Tradefed runs one thread per shard, so a thread-local variable to store which device is
  // being controlled at a time allows us to continue supporting device sharding while adding
  // post-test-class reboots.
  private static ThreadLocal<ITestDevice> sDevice = new ThreadLocal<ITestDevice>();

  /** Waits for device to be online, marks the most recent boottime of the device */
  @Before
  public void setUp() throws Exception {
    updateKernelStartTime();
    // TODO:(badash@): Watch for other things to track.
    //     Specifically time when app framework starts
  }

  /**
   * Makes sure the phone is online, and the ensure the current boottime is within 2 seconds (due to
   * rounding) of the previous boottime to check if The phone has crashed.
   */
  @After
  public void tearDown() throws Exception {
    getDevice().waitForDeviceAvailable(120 * 1000);
    assertTrue(
        "Phone has had a hard reset",
        (System.currentTimeMillis() / 1000 - getUptime() - kernelStartTime < 2));
    // TODO(badash@): add ability to catch runtime restart
    getDevice().disableAdbRoot();
  }


  /**
   * Reboot the device once a test class completes to shrink failure domains caused by unstable
   * tests.
   */
  @AfterClass
  public static void postTestReboot() throws Exception {
    // TODO(b/74410388): Remove this once stability has improved
    // Unable to use getDevice here due to static scope
    ITestDevice device = sDevice.get();
    if (device != null) {
      CLog.logAndDisplay(LogLevel.INFO, "Rebooting device " + device.getSerialNumber());
      device.rebootUntilOnline();
      device.waitForDeviceAvailable();
      CLog.logAndDisplay(LogLevel.INFO, "Device " + device.getSerialNumber() + " back online.");
    }
  }

  /** {@inheritDoc} */
  @Override
  public ITestDevice getDevice() {
    return sDevice.get();
  }

  /** {@inheritDoc} */
  @Override
  public void setDevice(ITestDevice device) {
    sDevice.set(device);
  }

  public void assertMatches(String pattern, String input) throws Exception {
    assertTrue("Pattern not found", Pattern.matches(pattern, input));
  }

  public void assertNotMatches(String pattern, String input) throws Exception {
    assertFalse("Pattern found", Pattern.matches(pattern, input));
  }

  public void assertNotMatchesMultiLine(String pattern, String input) throws Exception {
    assertFalse("Pattern found", Pattern.compile(pattern, Pattern.DOTALL).matcher(input).matches());
  }

  /** Check if a driver is present on a machine */
  protected boolean containsDriver(ITestDevice device, String driver) throws Exception {
    String result = device.executeShellCommand("ls -Zl " + driver);
    if (result.contains("No such file or directory")) {
      return false;
    }
    return true;
  }

  /**
   * Use {@link NativeDevice#enableAdbRoot()} internally.
   *
   * <p>The test methods calling this function should run even if enableAdbRoot fails, which is why
   * the return value is ignored. However, we may want to act on that data point in the future.
   */
  protected boolean enableAdbRoot(ITestDevice device) throws DeviceNotAvailableException {
    if (device.enableAdbRoot()) {
      return true;
    } else {
      CLog.w("\"enable-root\" set to false! Root is required to check if device is vulnerable.");
      return false;
    }
  }

  public long getUptime() throws Exception {
    String cmdOut = getDevice().executeShellCommand("cat /proc/uptime");
    if (cmdOut == null || cmdOut.indexOf(".") < 0) {
      getDevice().waitForDeviceAvailable(120 * 1000);
      cmdOut = getDevice().executeShellCommand("cat /proc/uptime");
    }

    return Long.parseLong(cmdOut.substring(0, cmdOut.indexOf('.')));
  }

  /** Allows a STS test to pass if called after a planned reboot. */
  protected void updateKernelStartTime() throws Exception {
    kernelStartTime = System.currentTimeMillis() / 1000 - getUptime();
  }
}
