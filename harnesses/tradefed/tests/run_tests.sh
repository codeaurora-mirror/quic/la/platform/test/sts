#!/bin/bash

# Copyright (C) 2018 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Common tools for running unit tests of the compatibility libs

JAR_DIR=${ANDROID_HOST_OUT}/framework

NEEDED_JARS="
    junit-host\
    tradefed\
    sts-harness-tradefed-tests"

checkFile() {
    if [ ! -f "$1" ]; then
        echo "Unable to locate $1"
        exit
    fi;
}

build_jar_path() {
    JAR_PATH=
    for JAR in ${NEEDED_JARS}; do
        checkFile ${1}/${JAR}.jar
        JAR_PATH=${JAR_PATH}:${1}/${JAR}.jar
    done
}

build_jar_path "${JAR_DIR}"
java -cp ${JAR_PATH} org.junit.runner.JUnitCore \
  com.android.tradefed.targetprep.StsDeviceInfoCollectorTest
