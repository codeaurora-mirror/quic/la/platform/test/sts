/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.tradefed.targetprep;

import com.android.ddmlib.Log.LogLevel;
import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.util.ArrayUtil;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Collects device info. This is a fork of VtsDeviceInfoCollector in order to associate user and
 * debug builds, critical for STS / APFE integration.
 */
public class StsDeviceInfoCollector implements ITargetPreparer, ITargetCleaner {

  // Primary key for build association, we need to override this.
  private static final String FINGERPRINT_KEY = "cts:build_fingerprint";

  // TODO(b/71637162): Dedupe this with the default CTS DeviceInfoCollector
  private static final Map<String, String> BUILD_KEYS = new HashMap<>();

  // List of keys copied from what CTS pulls in.
  static {
    BUILD_KEYS.put("cts:build_abi", "ro.product.cpu.abi");
    BUILD_KEYS.put("cts:build_abi2", "ro.product.cpu.abi2");
    BUILD_KEYS.put("cts:build_abis", "ro.product.cpu.abilist");
    BUILD_KEYS.put("cts:build_abis_32", "ro.product.cpu.abilist32");
    BUILD_KEYS.put("cts:build_abis_64", "ro.product.cpu.abilist64");
    BUILD_KEYS.put("cts:build_board", "ro.product.board");
    BUILD_KEYS.put("cts:build_brand", "ro.product.brand");
    BUILD_KEYS.put("cts:build_device", "ro.product.device");
    BUILD_KEYS.put(FINGERPRINT_KEY, "ro.build.fingerprint");
    BUILD_KEYS.put("cts:build_fingerprint_unaltered", "ro.vendor.build.fingerprint");
    BUILD_KEYS.put("cts:build_id", "ro.build.id");
    BUILD_KEYS.put("cts:build_manufacturer", "ro.product.manufacturer");
    BUILD_KEYS.put("cts:build_model", "ro.product.model");
    BUILD_KEYS.put("cts:build_product", "ro.product.name");
    BUILD_KEYS.put("cts:build_reference_fingerprint", "ro.build.reference.fingerprint");
    BUILD_KEYS.put("cts:build_serial", "ro.serialno");
    BUILD_KEYS.put("cts:build_tags", "ro.build.tags");
    BUILD_KEYS.put("cts:build_type", "ro.build.type");
    BUILD_KEYS.put("cts:build_version_base_os", "ro.build.version.base_os");
    BUILD_KEYS.put("cts:build_version_release", "ro.build.version.release");
    BUILD_KEYS.put("cts:build_version_sdk", "ro.build.version.sdk");
    BUILD_KEYS.put("cts:build_version_security_patch", "ro.build.version.security_patch");
    BUILD_KEYS.put("cts:build_version_incremental", "ro.build.version.incremental");
  }

  /** Determine if the property is empty or not. */
  private static boolean isEmptyProperty(String property) {
    return (property == null || property.trim().isEmpty() || property.equals("null"));
  }

  /** Generate a new build fingerprint by replacing the tags and type with user/release-keys. */
  public static String overrideBuildFingerprint(String oldFingerprint) throws TargetSetupError {
    if (isEmptyProperty(oldFingerprint)) {
      throw new TargetSetupError("Build fingerprint not found!");
    }
    // Extract and override values from build fingerprint.
    // Format from: https://source.android.com/compatibility/android-cdd
    // This may require corrections for older versions of Android.
    // $(BRAND)/$(PRODUCT)/$(DEVICE):$(VERSION.RELEASE)/$(ID)/
    //    $(VERSION.INCREMENTAL):$(TYPE)/$(TAGS)

    // Simple regex to verify the fingerprint matches expected formatting. Doesn't verify contents,
    // just format.
    if (!oldFingerprint.matches(".+/.+/.+:.+/.+/.+:.+/.+")) {
      throw new TargetSetupError("Build fingerprint does not match CDD pattern!");
    }

    int delimAfterIncremental = oldFingerprint.lastIndexOf(':');
    String unmodifiedSection = oldFingerprint.substring(0, delimAfterIncremental);

    String newFingerprint = unmodifiedSection + ":user/release-keys";

    String logMsg =
        "Overriding build fingerprint to: " + newFingerprint + ", was " + oldFingerprint;
    CLog.logAndDisplay(LogLevel.INFO, logMsg);
    return newFingerprint;
  }

  /** {@inheritDoc} */
  @Override
  public void setUp(ITestDevice device, IBuildInfo buildInfo)
      throws TargetSetupError, BuildError, DeviceNotAvailableException {
    for (Entry<String, String> entry : BUILD_KEYS.entrySet()) {
      String propertyValue = device.getProperty(entry.getValue());
      if (entry.getKey() == FINGERPRINT_KEY) {
        propertyValue = overrideBuildFingerprint(propertyValue);
      }
      buildInfo.addBuildAttribute(entry.getKey(), ArrayUtil.join(",", propertyValue));
    }
  }

  /** {@inheritDoc} */
  @Override
  public void tearDown(ITestDevice device, IBuildInfo buildInfo, Throwable e)
      throws DeviceNotAvailableException {}
}
