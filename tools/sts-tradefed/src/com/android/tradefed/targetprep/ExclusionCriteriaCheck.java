package com.android.tradefed.targetprep;

import com.android.compatibility.common.tradefed.targetprep.PreconditionPreparer;
import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.config.Option;
import com.android.tradefed.config.OptionClass;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;

/**
 * A preparer that enforces exclusion-criteria against a Device's properties
 */
@OptionClass(alias = "exclusion-criteria-check")
public class ExclusionCriteriaCheck extends PreconditionPreparer {
    @Option(name = "property-name",
            description = "The name of the property to check",
            mandatory = true)
    protected String mPropertyName = null;

    @Option(name = "exclusion-value",
            description = "Value for which the Device can't be allowed to proceed with the tests",
            mandatory = true)
    protected String mExclusionValue = null;

    @Option(name="throw-error",
    description = "Whether to throw an error if exclusion criteria matches")
    protected boolean mThrowError = true;

    @Override
    public void run(ITestDevice device, IBuildInfo buildInfo)
            throws TargetSetupError, BuildError, DeviceNotAvailableException {
        String propertyValue = device.getProperty(mPropertyName);
        if (mExclusionValue.equalsIgnoreCase(propertyValue)) {
            String errorMsg = String.format(
                    "Aborting as Device [%s] has Property \"%s\" with value \"%s\"",
                    device.getSerialNumber(),
                    mPropertyName,
                    propertyValue);
            if(mThrowError) {
                throw new TargetSetupError(errorMsg);
            } else {
                logWarning(errorMsg);
            }
        }
    }
}
